// C program to check if a given 
// year is leap year or not
#include <stdio.h>
#include <stdbool.h>

bool checkYear(int year)
{
    // If a year is multiple of 400, 
    // then it is a leap year
    if (year % 400 == 0)
        return true;

    // Else If a year is multiple of 100,
    // then it is not a leap year
    if (year % 100 == 0)
        return false;

    // Else If a year is multiple of 4,
    // then it is a leap year
    if (year % 4 == 0)
        return true;
    return false;
}

// Driver code
int main()
{
    int year[] = { 2000, 2001, 2002, 2003, 2004 };
    int arrLen = sizeof year / sizeof year[0];

    for (int i = 0; i < arrLen; i++)
    {
        if (checkYear(year[i])){
            printf("%u: Leap Year\n",year[i]);
        }else{
            printf("%u: Not a Leap Year\n",year[i]);
        }
    }
    return 0;
}

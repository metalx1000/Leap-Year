#!/usr/bin/nodejs
// program to check if a given 
// year is leap year or not

function checkYear(year)
{
    // If a year is multiple of 400, 
    // then it is a leap year
    if (year % 400 == 0)
        return true;

    // Else If a year is multiple of 100,
    // then it is not a leap year
    if (year % 100 == 0)
        return false;

    // Else If a year is multiple of 4,
    // then it is a leap year
    if (year % 4 == 0)
        return true;
    return false;
}

// Driver code
function main()
{
    let year = [ 2000, 2001, 2002, 2003, 2004 ];

    for (let i = 0; i < year.length; i++)
    {
        if (checkYear(year[i])){
            console.log(`${year[i]}: Leap Year`);
        }else{
            console.log(`${year[i]}: Not a Leap Year`);
        }
    }
    return 0;
}

main()

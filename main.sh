#!/bin/bash
#program to check if a given 
#year is leap year or not

function checkYear(){
    let year="$1"
    # If a year is multiple of 400, 
    # then it is a leap year
    [[ `expr $year % 400` == 0 ]] &&
        return 0;

    # Else If a year is multiple of 100,
    # then it is not a leap year
    [[ `expr $year % 100` == 0 ]] && 
        return 1;

    # Else If a year is multiple of 4,
    # then it is a leap year
    [[ `expr $year % 4` == 0 ]] &&
        return 0;
            return 1;
        }

# Driver code
function main()
{
    declare -a year=("2000" "2001" "2002" "2003" "2004")
    for (( i=0; i<${#year[@]}; i++ ));
    do
        if `checkYear "${year[i]}"`
        then
            echo "${year[i]}: Leap Year"
        else
            echo "${year[i]}: Not a Leap Year"
        fi
    done
    return 0;
}

main
